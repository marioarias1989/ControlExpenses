using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ControlExpenses.Interfaces;
using Xamarin.Forms;
using Plugin.CurrentActivity;

[assembly: Dependency(typeof(ControlExpenses.Droid.Extend.ShowToast))]

namespace ControlExpenses.Droid.Extend
{
    public class ShowToast : IShowToast
    {
        void IShowToast.ShowToast(string message)
        {
            Activity activity = CrossCurrentActivity.Current.Activity;
            Toast.MakeText(Forms.Context, message, ToastLength.Long).Show();
        }
    }
}