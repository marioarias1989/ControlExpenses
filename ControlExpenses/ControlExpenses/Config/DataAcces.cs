﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;
using Xamarin.Forms;
using System.IO;
using ControlExpenses.ViewModels;

namespace ControlExpenses.Config
{
    public class DataAcces : IDisposable
    {
        #region Variables

        private SQLiteConnection Connection;

        #endregion

        #region Builder

        public DataAcces()
        {
            var config = DependencyService.Get<IConfig>();
            Connection = new SQLiteConnection(config.Platform, Path.Combine(config.DirectoryDB, "ControlExpenses.db3"));
            Connection.CreateTable<BudgetIncomeViewModel>();
            Connection.CreateTable<BudgetDetailViewModel>();
            Connection.CreateTable<BudgetDetailExpensesViewModel>();
        }

        #endregion

        #region GenericMethods

        public void Insert<T>(T entity)
        {
            Connection.Insert(entity);
        }

        public void Update<T>(T entity)
        {
            Connection.Update(entity);
        }

        public void Delete<T>(T entity)
        {
            Connection.Delete(entity);
        }

        #endregion

        #region OthersMethods

        //public BudgetViewModel GetBudgetByMonthAndYear(int Month, int Year)
        //{
        //    return Connection.Table<BudgetViewModel>().FirstOrDefault(x => x.Month == Month && x.Year == Year);
        //}

        public List<BudgetIncomeViewModel> ListAllIncome()
        {
            return Connection.Table<BudgetIncomeViewModel>().OrderByDescending(x => x.Id).ToList();
        }

        public List<BudgetIncomeViewModel> ListIncomeByMonthAndYear(int year, int month)
        {
            return Connection.Table<BudgetIncomeViewModel>().Where(x => x.Year == year && x.Month == month).OrderByDescending(x => x.Id).ToList();
        }

        public decimal SumIncomeByMonthAndYear(int Month, int Year)
        {
            return (from x in Connection.Table<BudgetIncomeViewModel>() where x.Month == Month && x.Year == Year select x.Value).Sum();
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            Connection.Dispose();
        }

        #endregion
    }
}
