﻿using ControlExpenses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ControlExpenses.Config
{
    public static class MessageToast
    {
        public static void Message(string message)
        {
            if (Device.OS == TargetPlatform.Android)
            {
                DependencyService.Get<IShowToast>().ShowToast(message);
            }
        }
    }
}
