﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlExpenses.ViewModels
{
    public class BudgetIncomeViewModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
        public string MonthYear { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
}
