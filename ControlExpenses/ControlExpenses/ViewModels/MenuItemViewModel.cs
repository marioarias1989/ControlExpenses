﻿using ControlExpenses.Pages;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ControlExpenses.ViewModels
{
    public class MenuItemViewModel
    {
        public string Icon { get; set; }
        public string Title { get; set; }
        public string PageName { get; set; }

        public ICommand NavigateCommand
        {
            get { return new RelayCommand(Navigate); }
        }

        private void Navigate()
        {
            App.Master.IsPresented = false;
            switch (PageName)
            {
                case "MainPage":
                    App.Navigator.PopToRootAsync();
                    break;
                case "ListIncomePage":
                    var listIncomePage = new ListIncomePage();
                    NavigationPage.SetHasBackButton(listIncomePage, false);
                    App.Navigator.PushAsync(listIncomePage);
                    break;
                default:
                    break;
            }
        }
    }
}
