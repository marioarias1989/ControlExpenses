﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlExpenses.ViewModels
{
    public class BudgetDetailExpensesViewModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int BudgetDetailId { get; set; }
        public string Title { get; set; }
        public decimal Value { get; set; }
        public int Icon { get; set; }
    }
}
