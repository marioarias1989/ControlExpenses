﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlExpenses.Interfaces
{
    public interface IShowToast
    {
        void ShowToast(string message);
    }
}
