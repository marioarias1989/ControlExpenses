﻿using ControlExpenses.Config;
using ControlExpenses.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ControlExpenses.Pages
{
    public partial class ListIncomePage : ContentPage
    {
        #region Builder

        public ListIncomePage()
        {
            InitializeComponent();
            IncomePage_RefreshListView();
            this.tiItem.Clicked += TiItem_Clicked;
            this.lvItems.ItemSelected += LvItems_ItemSelected;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Llamado del evento
        /// </summary>
        private void IncomePage_RefreshListView()
        {
            using (var data = new DataAcces())
            {
                this.lvItems.ItemsSource = data.ListIncomeByMonthAndYear(App.Year, App.Month);
                this.lvItems.Unfocus();
            }
        }
        
        /// <summary>
        /// Método que abre la página de ingresos ya sea para guardar o actualizar
        /// </summary>
        private void OpenIncomePage(BudgetIncomeViewModel entity)
        {
            var incomePage = new IncomePage(entity);
            incomePage.RefreshListView += IncomePage_RefreshListView;
            App.Navigator.PushAsync(incomePage);
        }

        #endregion

        #region Handlers

        /// <summary>
        /// Evento que se dispara al presionar sobre el boton de la barra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TiItem_Clicked(object sender, EventArgs e)
        {
            OpenIncomePage(null);
        }

        /// <summary>
        /// Evento que se dispara al seleccionar un item del listado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvItems_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            OpenIncomePage((BudgetIncomeViewModel)e.SelectedItem);
        }

        #endregion
    }
}
