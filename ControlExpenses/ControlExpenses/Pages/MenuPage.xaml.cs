﻿using ControlExpenses.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ControlExpenses.Pages
{
    public partial class MenuPage : ContentPage
    {
        List<MenuItemViewModel> ListDatasource;

        public MenuPage()
        {
            InitializeComponent();
            LoadMenu();
        }

        private void LoadMenu()
        {
            ListDatasource = new List<MenuItemViewModel>();
            ListDatasource.Add(new MenuItemViewModel { Icon = "ic_action_principal", Title = "Principal", PageName = "MainPage" });
            ListDatasource.Add(new MenuItemViewModel { Icon = "ic_action_income", Title = "Ingresos", PageName = "ListIncomePage" });
            ListDatasource.Add(new MenuItemViewModel { Icon = "ic_action_expense", Title = "Gastos", PageName = "" });
            ListDatasource.Add(new MenuItemViewModel { Icon = "ic_action_setting", Title = "Configuración", PageName = "SettingsPage" });
            this.lvMenu.ItemsSource = ListDatasource;
        }
    }
}
