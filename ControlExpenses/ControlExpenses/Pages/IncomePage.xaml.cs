﻿using ControlExpenses.Config;
using ControlExpenses.Interfaces;
using ControlExpenses.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ControlExpenses.Pages
{
    public partial class IncomePage : ContentPage
    {
        #region Builder

        public IncomePage(BudgetIncomeViewModel _budgetIncomeViewModel)
        {
            InitializeComponent();
            this.tiItem.Clicked += TiItem_Clicked;
            this.btnDelete.Clicked += BtnDelete_Clicked;
            this.BudgetIncomeViewModel = _budgetIncomeViewModel;
            LoadControls();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Representa a la entidad de ingresos
        /// </summary>
        private BudgetIncomeViewModel BudgetIncomeViewModel { get; set; }

        #endregion

        #region Event

        public delegate void RefreshDatasource();
        public event RefreshDatasource RefreshListView;
        
        #endregion

        #region Methods

        /// <summary>
        /// Guarda o actualiza la entidad
        /// </summary>
        private void SaveBudgetIncome()
        {
            //Si la descripción no está diligenciada
            if (this.txtDescription.Text == string.Empty)
            {
                MessageToast.Message("Debe ingresar una descripción");
                return;
            }

            //Si el valor no está diligenciado
            if (this.txtValue.Text == string.Empty || Convert.ToDecimal(txtValue.Text) == 0)
            {
                MessageToast.Message("Debe ingresar un valor");
                return;
            }

            //Si pasa todas las validaciones se procede a guardar el ingreso
            using (var data = new DataAcces())
            {
                //Se crea la entidad que va a ser guardada si viene para guardar
                if (this.BudgetIncomeViewModel == null)
                {
                    this.BudgetIncomeViewModel = new BudgetIncomeViewModel();
                }
                this.BudgetIncomeViewModel.Description = txtDescription.Text;
                this.BudgetIncomeViewModel.Value = Convert.ToDecimal(txtValue.Text);
                this.BudgetIncomeViewModel.MonthYear = App.MonthYear;
                this.BudgetIncomeViewModel.Month = App.Month;
                this.BudgetIncomeViewModel.Year = App.Year;

                if (this.BudgetIncomeViewModel.Id == 0)
                {
                    data.Insert<BudgetIncomeViewModel>(this.BudgetIncomeViewModel);
                    MessageToast.Message("Guardado Correctamente");
                    this.txtDescription.Text = string.Empty;
                    this.txtValue.Text = string.Empty;
                    this.txtDescription.Unfocus();
                    this.txtValue.Unfocus();
                    RefreshListView();
                    this.BudgetIncomeViewModel = null;
                }
                else
                {
                    data.Update<BudgetIncomeViewModel>(this.BudgetIncomeViewModel);
                    MessageToast.Message("Actualizado Correctamente");
                    RefreshListView();
                    App.Navigator.PopAsync();
                }
            }
        }

        /// <summary>
        /// Carga los controles
        /// </summary>
        private void LoadControls()
        {
            if (this.BudgetIncomeViewModel != null) //Si no es null es porque se va a editar
            {
                this.txtDescription.Text = this.BudgetIncomeViewModel.Description;
                this.txtValue.Text = Convert.ToString(this.BudgetIncomeViewModel.Value);
                this.slDelete.IsVisible = true;
            }
            else //Si se va a guardar oculta el botón de eliminar
            {
                this.slDelete.IsVisible = false;
            }
        }

        #endregion

        #region Handlers

        /// <summary>
        /// Evento que se dispara al presionar el boton de guardar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TiItem_Clicked(object sender, EventArgs e)
        {
            SaveBudgetIncome();
        }

        /// <summary>
        /// Evento que se dispara al presionar click sobre el botón de eliminar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDelete_Clicked(object sender, EventArgs e)
        {
            using (var data = new DataAcces())
            {
                data.Delete<BudgetIncomeViewModel>(this.BudgetIncomeViewModel);
                MessageToast.Message("Eliminado Correctamente");
                RefreshListView();
                App.Navigator.PopAsync();
            }
        }

        #endregion

    }
}
