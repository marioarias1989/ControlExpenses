﻿using ControlExpenses.Config;
using ControlExpenses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ControlExpenses.Pages
{
    public partial class MainPage : ContentPage
    {
        #region Builder

        public MainPage()
        {
            InitializeComponent();
            this.btnLeft.Clicked += BtnLeft_Clicked;
            this.btnMonthYear.Clicked += BtnMonthYear_Clicked;
            this.btnRigth.Clicked += BtnRigth_Clicked;
            LoadListMonths();
            Month = DateTime.Now.Month;
            Year = DateTime.Now.Year;
            CalculateMonthYear(true, true);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Mes escogido por el usuario
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// Año escogido por el usuario
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Listado de meses
        /// </summary>
        public List<Tuple<int, string>> ListMonths;

        #endregion

        #region Handlers

        /// <summary>
        /// Evento que se dispara al presionar click sobre el boton de antes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnLeft_Clicked(object sender, EventArgs e)
        {
            CalculateMonthYear(false);
        }

        /// <summary>
        /// Evento que se dispara al presionar click sobre el boton de despues
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnRigth_Clicked(object sender, EventArgs e)
        {
            CalculateMonthYear(true);
        }

        /// <summary>
        /// Evento que se dispara al presionar click sobre el boton de mes y año
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMonthYear_Clicked(object sender, EventArgs e)
        {
            App.Navigator.PushAsync(new SelectMonthYearPage(), false);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carga el listado de meses
        /// </summary>
        private void LoadListMonths()
        {
            ListMonths = new List<Tuple<int, string>>();
            ListMonths.Add(new Tuple<int, string>(01, "Enero"));
            ListMonths.Add(new Tuple<int, string>(02, "Febrero"));
            ListMonths.Add(new Tuple<int, string>(03, "Marzo"));
            ListMonths.Add(new Tuple<int, string>(04, "Abril"));
            ListMonths.Add(new Tuple<int, string>(05, "Mayo"));
            ListMonths.Add(new Tuple<int, string>(06, "Junio"));
            ListMonths.Add(new Tuple<int, string>(07, "Julio"));
            ListMonths.Add(new Tuple<int, string>(08, "Agosto"));
            ListMonths.Add(new Tuple<int, string>(09, "Septiembre"));
            ListMonths.Add(new Tuple<int, string>(10, "Octubre"));
            ListMonths.Add(new Tuple<int, string>(11, "Noviembre"));
            ListMonths.Add(new Tuple<int, string>(12, "Diciembre"));
        }

        /// <summary>
        /// Calcula el valor del mes y del año
        /// </summary>
        private void CalculateMonthYear(bool IsNext, bool IsInitialDate = false)
        {
            if (Month == 12 && IsNext == true && IsInitialDate == false)
            {
                Month = 01;
                Year += 1;
            }
            else if (Month == 01 && IsNext == false && IsInitialDate == false)
            {
                Month = 12;
                Year -= 1;
            }
            else if (IsInitialDate == false)
            {
                if (IsNext == true)
                {
                    Month += 1;
                }
                else
                {
                    Month -= 1;
                }
            }
            var info = (from x in ListMonths where x.Item1 == Month select x).FirstOrDefault();
            this.btnMonthYear.Text = info.Item2 + " " + Year.ToString();

            //Se asignan las variables que se utilizan en toda la app de manera global
            App.MonthYear = info.Item2 + " " + Year.ToString();
            App.Month = Month;
            App.Year = Year;
            LoadInfo();
        }

        /// <summary>
        /// Carga la información pertinente a los totales
        /// </summary>
        private void LoadInfo()
        {
            using (var data = new DataAcces())
            {
                //Total de ingresos
                var sumIncome = data.SumIncomeByMonthAndYear(Month, Year);
                this.lblTotalIncome.Text = sumIncome.ToString("C0");
            }
        }

        /// <summary>
        /// Metodo que se ejecuta cada vez que el MainPage aparece
        /// </summary>
        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadInfo();
        }

        #endregion
    }
}
